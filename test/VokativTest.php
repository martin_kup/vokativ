<?php declare(strict_types=1);

namespace Vokativ\Test;

use PHPUnit\Framework\TestCase;
use Vokativ\Name;

class VokativTest extends TestCase
{
    private const VOKATIV_TEST_DIR = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
    protected Name $_v;

    protected function loadTests(string $name): array
    {
        $filename = self::VOKATIV_TEST_DIR . $name . '.txt';
        $f = fopen($filename, 'rb');
        $tests = [];

        /** @noinspection PhpAssignmentInConditionInspection */
        while ($line = rtrim((string)fgets($f))) {
            if ($line !== '') {
                $tests[] = explode(' ', $line, 2);
            }
        }

        fclose($f);

        return $tests;
    }

    public function setUp(): void
    {
        $this->_v = new Name();
    }

    public function testBasics(): void
    {
        static::assertTrue($this->_v->isMale('Tom'));
        static::assertEquals('tome', $this->_v->vokativ('Tom'));
        static::assertEquals('tome', $this->_v->vokativ('toM'));
        static::assertEquals('tome', $this->_v->vokativ('ToM'));
        static::assertIsString($this->_v->vokativ('Tom'));
    }

    public function testManFirstNames(): void
    {
        foreach (
            $this->loadTests('man_first_name_tests') as [$name, $vok]) {

            static::assertEquals($vok, $this->_v->vokativ($name, false, false));
            static::assertEquals($vok, $this->_v->vokativ($name, null, false));
            static::assertEquals($vok, $this->_v->vokativ($name, false));
            static::assertEquals($vok, $this->_v->vokativ($name));
            static::assertTrue($this->_v->isMale($name));
        }
    }

    public function testManLastNames(): void
    {
        foreach (
            $this->loadTests('man_last_name_tests') as [$name, $vok]) {

            static::assertEquals($vok, $this->_v->vokativ($name, false, true));
            static::assertEquals($vok, $this->_v->vokativ($name, null, true));
            static::assertEquals($vok, $this->_v->vokativ($name, false));
            static::assertEquals($vok, $this->_v->vokativ($name));
            static::assertTrue($this->_v->isMale($name));
        }
    }

    public function testWomanFirstNames(): void
    {
        foreach (
            $this->loadTests('woman_first_name_tests') as [$name, $vok]) {

            static::assertEquals($vok, $this->_v->vokativ($name, true, false));
            static::assertEquals($vok, $this->_v->vokativ($name, null, false));
            static::assertEquals($vok, $this->_v->vokativ($name, true));
            static::assertEquals($vok, $this->_v->vokativ($name));
            static::assertFalse($this->_v->isMale($name));
        }
    }

    public function testWomanLastNames(): void
    {
        foreach (
            $this->loadTests('woman_last_name_tests') as [$name, $vok]) {

            static::assertEquals($vok, $this->_v->vokativ($name, true, true));
            static::assertEquals($vok, $this->_v->vokativ($name, null, true));
            static::assertEquals($vok, $this->_v->vokativ($name, true));
            static::assertEquals($vok, $this->_v->vokativ($name));
            static::assertFalse($this->_v->isMale($name));
        }
    }
}
