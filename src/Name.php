<?php

declare(strict_types=1);

namespace Vokativ;

use RuntimeException;

use function array_key_exists;
use function file_exists;
use function file_get_contents;
use function mb_strlen;
use function mb_strtolower;
use function mb_substr;
use function range;
use function unserialize;

use const DIRECTORY_SEPARATOR;

class Name
{
    private const VOKATIV_DATA_DIR = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;

    /**
     * @var string[]|null
     */
    protected ?array $_manSuffixes = null;

    /**
     * @var string[]|null
     */
    protected ?array $_manVsWomanSuffixes = null;

    /**
     * @var string[]|null
     */
    protected ?array $_womanFirstVsLastSuffixes = null;

    /**
     * Vrací jméno vyskloňované do 5. pádu
     *
     * @param string    $name
     * @param bool|null $isWoman
     * @param bool|null $isLastName
     *
     * @return string
     */
    public function vokativ(string $name, ?bool $isWoman = null, ?bool $isLastName = null): string
    {
        $name = mb_strtolower($name, 'UTF-8');

        if ($isWoman === null) {
            $isWoman = !$this->isMale($name);
        }

        if ($isWoman) {
            if ($isLastName === null) {
                [$match, $type] = $this->getMatchingSuffix($name, $this->getWomanFirstVsLastNameSuffixes());
                $isLastName = $type === 'l';
            }

            if ($isLastName) {
                return $this->vokativWomanLastName($name);
            }

            return $this->vokativWomanFirstName($name);
        }

        return $this->vokativMan($name);
    }

    /**
     * Na základě jména nebo přijmení rozhodne o pohlaví
     *
     * @param string $name
     *
     * @return bool
     */
    public function isMale(string $name): bool
    {
        $name = mb_strtolower($name, 'UTF-8');
        [$match, $sex] = $this->getMatchingSuffix($name, $this->getManVsWomanSuffixes());

        return $sex !== 'w';
    }

    protected function vokativMan(string $name): string
    {
        [$match, $suffix] = $this->getMatchingSuffix($name, $this->getManSuffixes());

        if ($match) {
            $name = mb_substr($name, 0, -1 * mb_strlen($match));
        }

        return $name . $suffix;
    }

    protected function vokativWomanFirstName(string $name): string
    {
        if (mb_substr($name, -1) === 'a') {
            return mb_substr($name, 0, -1) . 'o';
        }

        return $name;
    }

    protected function vokativWomanLastName(string $name): string
    {
        return $name;
    }

    /**
     * @param string   $name
     * @param string[] $suffixes
     *
     * @return string[]
     */
    protected function getMatchingSuffix(string $name, array $suffixes): array
    {
        // it is important(!) to try suffixes from longest to shortest
        foreach (range(mb_strlen($name), 1) as $length) {
            $suffix = mb_substr($name, -1 * $length);

            if (array_key_exists($suffix, $suffixes)) {
                return [$suffix, $suffixes[$suffix]];
            }
        }

        return ['', $suffixes['']];
    }

    /**
     * @return string[]
     */
    protected function getManSuffixes(): array
    {
        if ($this->_manSuffixes === null) {
            $this->_manSuffixes = $this->readSuffixes('man_suffixes');
        }

        return $this->_manSuffixes;
    }

    /**
     * @return string[]
     */
    protected function getManVsWomanSuffixes(): array
    {
        if ($this->_manVsWomanSuffixes === null) {
            $this->_manVsWomanSuffixes = $this->readSuffixes('man_vs_woman_suffixes');
        }

        return $this->_manVsWomanSuffixes;
    }

    /**
     * @return string[]
     */
    protected function getWomanFirstVsLastNameSuffixes(): array
    {
        if ($this->_womanFirstVsLastSuffixes === null) {
            $this->_womanFirstVsLastSuffixes = $this->readSuffixes('woman_first_vs_last_name_suffixes');
        }

        return $this->_womanFirstVsLastSuffixes;
    }

    /**
     * @param string $file
     *
     * @return string[]
     */
    protected function readSuffixes(string $file): array
    {
        $filename = self::VOKATIV_DATA_DIR . $file;

        if (!file_exists($filename)) {
            throw new RuntimeException('Data file ' . $filename . 'not found');
        }

        return unserialize(file_get_contents($filename), ['allowed_classes' => false]);
    }
}
